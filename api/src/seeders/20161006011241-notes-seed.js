'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
   
	// Define dummy data for posts
	const notes = [
	{
		title: 'A silly post',
		content: 'Roses are red, violets are blue, I am a poet.',
		notebookId: "1",
		createdAt: new Date('2010/08/17 12:09'),
		updatedAt: new Date('2010/08/17 12:09')
	},
	{
		title: 'New technology',
		content: 'These things called "computers" are pretty fancy.',
		notebookId: "2",
		createdAt: new Date('2011/03/06 15:32'),
		updatedAt: new Date('2011/03/06 15:47')
	},
	{
		title: ' 2New technology2',
		content: '2 These things called "computers" are pretty fancy 2.',
		notebookId: "2",
		createdAt: new Date('2011/03/06 15:33'),
		updatedAt: new Date('2011/03/06 15:48')
	}
	];
	// Insert posts into the database
		return queryInterface.bulkInsert('Notes', notes, {});

   
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('Person', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
  },

  down: function (queryInterface, Sequelize) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
     */ 
	return queryInterface.bulkDelete('Notes', null, {});
    
  }
};
