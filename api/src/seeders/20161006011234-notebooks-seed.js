'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
	// Define dummy data for posts
	const notebooks = [
	{
		title: 'A silly post',
		createdAt: new Date('2010/08/17 12:09'),
		updatedAt: new Date('2010/08/17 12:09')
	},
	{
		title: 'New technology',
		createdAt: new Date('2011/03/06 15:32'),
		updatedAt: new Date('2011/03/06 15:47')
	},
  {
		title: 'Do good',
		createdAt: new Date('2010/09/17 12:09'),
		updatedAt: new Date('2010/05/17 12:09')
	},
	{
		title: 'Many thanks',
		createdAt: new Date('2011/05/06 15:32'),
		updatedAt: new Date('2011/05/06 15:47')
	},
  {
		title: 'Bills bad',
		createdAt: new Date('2010/08/17 12:45'),
		updatedAt: new Date('2010/08/17 12:34')
	},
	{
		title: 'Tuff stuff',
		createdAt: new Date('2011/03/14 15:32'),
		updatedAt: new Date('2011/03/14 15:47')
	},
  {
		title: 'Good times',
		createdAt: new Date('2010/08/22 12:09'),
		updatedAt: new Date('2010/08/11 12:09')
	},
	{
		title: 'Bad Santa',
		createdAt: new Date('2011/03/12 15:32'),
		updatedAt: new Date('2011/03/45 15:47')
	}
	];
	// Insert posts into the database
		return queryInterface.bulkInsert('Notebooks', notebooks, {});
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkInsert('Person', [{
        name: 'John Doe',
        isBetaMember: false
      }], {});
    */
  },

  down: function (queryInterface, Sequelize) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.

      Example:
      return queryInterface.bulkDelete('Person', null, {});
    */
	return queryInterface.bulkDelete('Notebooks', null, {});
  }
};
