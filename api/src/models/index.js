const Sequelize = require('sequelize');

// Load our database configuration
const dbConfig = require('../config/database');

// Connect Sequelize to the database
const sequelize = new Sequelize(dbConfig.database, dbConfig.user,
  dbConfig.password, dbConfig);

// Load all of our model definitions
const models = {
  /* *** TODO: Import your models here *** */
  // eg. `Note: sequelize.import(require.resolve('./note'))` if you have a model in models/note.js
  Note: sequelize.import(require.resolve('./note')),
  Notebook: sequelize.import(require.resolve('./notebook'))
};

/* *** TODO: Set up Sequelize associations here *** */
models.Notebook.hasMany(models.Note, {
  onDelete: 'CASCADE',
  hooks: true,
  foreignKey: 'notebookId'
});
models.Note.belongsTo(models.Notebook, {
  onDelete: 'CASCADE',
  foreignKey: 'notebookId'
});

// Store the database connection (used in tests)
models.database = sequelize;

// Export our model definitions
module.exports = models;
