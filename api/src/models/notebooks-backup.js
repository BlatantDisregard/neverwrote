'use strict';
modul.exports = {
  up: function(queryInterface, Sequelize){
    return queryInterface.createTable('Notebooks', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequeilze.INTEGER
        },
       title: {
         allowNull: false,
         type: Sequlize.STRING
        }
    });
  },
  down: function(queryInterface, Sequelize){
    return queryInterface.dropTable('Notebooks');
  }
};


docker-compose run --rm api sequelize model:create --name Notebook --attributes title:string

docker-compose run --rm api sequelize model:create --name Note --attributes title:string,content:text,notebookId:integer