'use strict';
module.exports = {
  up: function(queryInterface, Sequelize){
    return queryInterface.createTable('Notes', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: Sequeilze.INTEGER
        },
       title: {
         allowNull: false,
         type: Sequlize.STRING
        },
       content:{
         type: Sequelize.STRING
       }
    });
  },
  down: function(queryInterface, Sequelize){
    return queryInterface.dropTable('Notes');
  }
};