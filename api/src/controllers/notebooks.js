const express = require('express');
const _ = require('lodash');
const models = require('../models');
const router = express.Router();

/* *** TODO: Fill in the API endpoints for notebooks *** */

// GET /notebooks = Show all
router.get('/', (req, res) => {
  models.Notebook.findAll()
  .then(notebook => res.json(notebook))
  .catch(err => res.status(500).json({ error: err.message }));
});

// For GET /notebooks/:notebookId/notes = Show by id > all notes
router.get('/:notebookId/notes', (req, res) => {
  models.Note.findAll({ where: { notebookId: req.params.notebookId } })
  .then(note => res.json(note))
  .catch(err => res.status(500).json({ error: err.message }));
});
// For POST /notebooks = Create
router.post('/', (req, res) => {
  models.Notebook.create(req.body)
  .then(notebook => res.json(notebook))
  .catch(err => res.status(500).json({ error: err.message }));
});
//For GET /notebooks/:notebookId = find by id
router.get('/:notebookId', (req, res) => {
  models.Notebook.findById(req.params.notebookId)
  .then(notebook => res.json(notebook))
  .catch(err => res.status(500).json({ error: err.message }));
});
// For DELETE /notebooks/:notebookId
router.delete('/:notebookId', (req, res) => {
  models.Notebook.destroy({ where: { id: req.params.notebookId } })
  .then(() => res.json({}))
  .catch(err => res.status(500).json({ error: err.message }));
});
// For PUT /notebooks/:notebookId = update
router.put('/:notebookId', (req, res) => {
  models.Notebook.findById(req.params.notebookId)
  .then(notebook => notebook.updateAttributes(req.body))
  .then(notebook => res.json(notebook))
  .catch(err => res.status(500).json({ error: err.message }));
});

module.exports = router;