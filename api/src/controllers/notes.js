const express = require('express');
const _ = require('lodash');
const models = require('../models');
const router = express.Router();

/* *** TODO: Fill in the API endpoints for notes *** */
// For GET /notes = Show all
router.get('/', (req, res) => {
  models.Note.findAll()
  .then(note => res.json(note))
  .catch(err => res.status(400).json({ error: err.message }));
});
//For POST /notes = Create
router.post('/', (req, res) => {
  models.Note.create(req.body)
  .then(note => res.json(note))
  .catch(err => res.status(420).json({ error: err.message }));
});
//For GET /notes/:noteId = find by id
router.get('/:noteId', (req, res) => {
  models.Note.findById(req.params.noteId)
  .then(note => res.json(note))
  .catch(err => res.status(430).json({ error: err.message }));
});
// For DELETE /notes/:noteId
router.delete('/:noteId', (req, res) => {
  models.Note.destroy({ where: { id: req.params.noteId } })
  .then(() => res.json({}))
  .catch(err => res.status(440).json({ error: err.message }));
});
//For PUT /notes/:noteId = update
router.put('/:noteId', (req, res) => {
  models.Note.findById(req.params.noteId)
  .then(note => note.updateAttributes(req.body))
  .then(note => res.json(note))
  .catch(err => res.status(350).json({ error: err.message }));
});

module.exports = router;

/*
const
const
const
const
express = require('express');
_ = require('lodash');
models = require('../models');
router = express.Router();
// This helper function takes the JSON object submitted in a request and
// selects only the fields that are allowed to be set by users
function postFilter(obj) {
return _.pick(obj, ['title', 'content']);
}
// Index
10router.get('/', (req, res) => {
// Return a list of the five most recent posts
const queryOptions = {
order: [['createdAt', 'DESC']],
limit: 5
};
models.Post.findAll(queryOptions)
.then(posts => res.json(posts))
.catch(err => res.status(500).json({ error: err.message }));
});
// Create
router.post('/', (req, res) => {
// Create a new post record in the database
models.Post.create(postFilter(req.body))
.then(post => res.json(post))
.catch(err => res.status(422).json({ error: err.message }));
});
// Show
router.get('/:postId', (req, res) => {
// Return the specified post record from the database
models.Post.findById(req.params.postId)
.then(post => res.json(post))
.catch(err => res.status(500).json({ error: err.message }));
});
// Destroy
router.delete('/:postId', (req, res) => {
// Delete the specified post record from the database
models.Post.destroy({ where: { id: req.params.postId } })
.then(() => res.json({}))
.catch(err => res.status(500).json({ error: err.message }));
});
// Update
// TODO: Implement the update action here
module.exports = router;*/