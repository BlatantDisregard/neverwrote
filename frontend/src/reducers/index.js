/**
 * Specify all of your reducers in this file, so they can be combined into
 * one big reducer.
 */

const Redux = require('redux');

const notebooks = require('./notebooks');
const notes = require('./notes');

module.exports = Redux.combineReducers({
  /* *** TODO: Put your reducers in here *** */
  // eg. `notes: require('./notes')` if you have a reducer in reducers/notes.js
  notebooks,
  notes
});
